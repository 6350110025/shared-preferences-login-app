import 'package:flutter/material.dart';

class textfieldpwd extends StatelessWidget {
  const textfieldpwd({
    Key? key,
    required this.data,
    required this.field,
  }) : super(key: key);

  final TextEditingController data;
  final String field;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: data,
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: field,
        hintText: 'Please enter your $field',
      ),
    );
  }
}