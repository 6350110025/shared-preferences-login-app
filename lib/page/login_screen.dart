import 'package:flutter/material.dart';
import 'package:login_prefs/src/text_field_email.dart';
import 'package:login_prefs/src/text_field_password.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key, required this.title, }) : super(key: key);
  final String title;

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPassword = TextEditingController();

  late SharedPreferences prefs;
  String name = "";

  @override
  void initState() {
    super.initState();
    _dataLogin();
  }

  void _dataLogin() async {
    prefs = await SharedPreferences.getInstance();
    name = prefs.getString("Email") ?? '';
    setState(() {
      userEmail.text = name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 30,left: 35,right: 35),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    Image.network(
                      'https://upload.wikimedia.org/wikipedia/commons/1/17/Google-flutter-logo.png',
                        width: 250,
                    ),
                    SizedBox(
                      height: 70,
                    ),
                    textfieldemail(
                      data: userEmail,
                      field: 'Email',
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    textfieldpwd(
                      data: userPassword,
                      field: 'Password',
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 18),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ButtonTheme(
                            minWidth: 300.0,
                            height: 50.0,
                            child: RaisedButton(
                              onPressed: () => login(),
                              child: Text(
                                "Login",
                                style: TextStyle(
                                    fontSize: 24, color: Colors.white),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(14.0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///Design Toast
  void login() async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString("Email", userEmail.text.toString());


    await Fluttertoast.showToast(
        msg: ' You are Login Successfully. 🥳',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 3,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 18.0);
  }
}








